## Common rules

- Party needs to check with satellite whether it's a valid party  
- Party needs to check certificate is valid 
- All JWT tokens are signed using a JSON Web Signature (JWS)


## Authentication and authorization 

### 1. Service Consumer is Entitled Party

Conflicting information:

1) Provider keeps their own entitlement information. 
   Hence, they do not need to access external sources for authentication an authorization.
   [Functional documentation](https://ishareworks.atlassian.net/wiki/spaces/IS/pages/70222085/1.+M2M+service+provision)
2) Provider checks with satellite as in sequence diagram below.
   [Technical documentation](https://dev.ishare.eu/reference/authentication.html)
```mermaid
---
title: Service Consumer being Entitled Party
---
sequenceDiagram
    participant Consumer
    participant Provider
    participant Satellite
    activate Consumer
    Consumer->>Consumer: create JWT signed with own PKI Certificate
    Consumer->>+Provider: /connect/token with JWT
    
    Provider ->> Provider: validate JWT
    Note over Provider: validate signature<br/>check with trusted CAs (from satellite)<br/>validate certificate<br/>

    Provider->>Provider: create JWT signed with own PKI Certificate
    Provider->>+Satellite: /connect/token with JWT
    Satellite ->> Satellite: validate JWT
    Satellite-->>-Provider: return access token
    Provider->>+Satellite: /parties/<eori> (incl. access token)
    Satellite-->>-Provider: return party (meaning it's authenticated)
    
    Provider-->>-Consumer: return access token
    Consumer->>+Provider: /some/data/endpoint (incl. access token)
    Provider-->>-Consumer: return result
    deactivate Consumer
```

### 2. Service Consumer entitled by Entitled Party

Provider needs Delegation Evidence to proof that Consumer is delegated by Entitled Party.
Either Consumer provides this evidence (given by Entitled Party), 
OR Provider already has evidence (given by Entitled Party),
OR the Provider checks in Authorization Registry (registered by Entitled Party).

However, the technical documentation states that the consumer registers the evidence at the Authorization Registry will fetch it.
[See scenario 4](https://dev.ishare.eu/service-consumer/getting-started.html#scenario-4-full-implementation-with-prechecks-and-delegation-evidence).



## Usage of Service Provider

```mermaid
---
title: Sequence diagram Consumer Provider minimal interaction
---
sequenceDiagram
    participant Consumer
    participant Provider
    activate Consumer
    Consumer->>Consumer: create JWT signed with own PKI Certificate
    Consumer->>+Provider: /connect/token with JWT
    Provider ->> Provider: validate JWT (TODO: how??)
    Provider-->>-Consumer: return access token
    Consumer->>+Provider: /some/data/endpoint (incl. access token)
    Provider-->>-Consumer: return result
    deactivate Consumer
```


```mermaid
---
title: Sequence diagram Consumer Provider complete interaction
---
sequenceDiagram
    participant Consumer
    participant Provider
    participant Satellite
    activate Consumer
    Consumer ->> Consumer: create JWT signed with own PKI Certificate
    Note over Consumer, Satellite: Find and get information about parties [Optional]
    rect white
        Note over Consumer, Satellite: Authenticate
        Consumer ->>+ Satellite: /connect/token with JWT
        Satellite ->> Satellite: validate JWT (See Authentication and authorization)
        Satellite -->>- Consumer: return access token
        Note over Consumer, Satellite: Obtain the iSHARE list of trusted certificate authorities (currently, eIDAS certificate issuer certificate authorities).
        Consumer ->>+ Satellite: /trusted_list (incl. access token)
        Satellite -->>- Consumer: return tusted parties
        Note over Consumer, Satellite: search for iSHARE participants from your iSHARE Satellite.
        Consumer ->>+ Satellite: /parties (incl. access token)
        Satellite -->>- Consumer: return list of parties
        Note over Consumer, Satellite: Obtain information about one iSHARE participant from the iSHARE Satellite.
        Consumer ->>+ Satellite: /parties/<eori> (incl. access token)
        Satellite -->>- Consumer: return party

        Consumer ->> Consumer: check if party has expected role
        Consumer ->> Consumer: check if party's certifcate is valid (on trusted list)
    end
    Note over Consumer,  Provider: Getting information and data from Provider
    rect white
        Note over Consumer, Provider: Authenticate
        Consumer ->>+ Provider: /connect/token with JWT
        Provider ->> Provider: validate JWT (See Authentication and authorization)
        Provider -->>- Consumer: return access token
        Note over Consumer, Provider:get capabilites of Provider
        Consumer ->>+ Provider: /capabilities (incl. access token)
        Provider -->>- Consumer: return capabilities
        Note over Consumer, Provider: Use some restricted service from Provider
        Consumer ->>+ Provider: /some/data/endpoint (incl. access token)
        Provider -->>- Consumer: return result
    end
    deactivate Consumer
```

## Delegation
Entitled Party (EP) needs to register delegation at Service Provider, Entitled Party or Authorization Registry.
Delegated Consumer accesses Provider with Delegation Evidence.
Provider decides based on delegation evidence whether to give delegated consumer access or not. 

```mermaid
---
title: Delegation evidence sent by Consumer
---
sequenceDiagram
    participant Consumer
    participant Provider
    participant Authorization Registry
    activate Consumer
    Consumer ->> Consumer: create JWT signed with own PKI Certificate
    Consumer ->>+ Authorization Registry: /connect/token with JWT
    Authorization Registry ->> Authorization Registry: validate JWT (See Authentication and authorization)
    Authorization Registry -->>- Consumer: return access token
    Consumer ->>+ Authorization Registry: /delegation (inlc. access token)
    Authorization Registry -->>- Consumer: return JWT with delegation evidence
    Consumer ->>+ Provider: /connect/token with JWT with delegation evidence
    Provider ->> Provider: validate JWT 
    Provider -->>- Consumer: return access token
    Note over Consumer, Provider: Use some restricted service from Provider
    Consumer ->>+ Provider: /some/data/endpoint (incl. access token)
    Provider -->>- Consumer: return result
    deactivate Consumer
    
```
```mermaid
---
title: Delegation evidence fetched by Provider
---
sequenceDiagram
    participant Consumer
    participant Provider
    participant Authorization Registry
    activate Consumer
    Consumer ->> Consumer: create JWT signed with own PKI Certificate
    Consumer ->>+ Provider: /connect/token with JWT 
    Provider ->> Provider: validate JWT
    Provider ->>+ Authorization Registry: /connect/token with JWT
    Authorization Registry ->> Authorization Registry: validate JWT (See Authentication and authorization)
    Authorization Registry -->>- Provider: return access token
    Provider ->>+ Authorization Registry: /delegation (inlc. access token)
    Authorization Registry -->>- Provider: return JWT with delegation evidence
    Provider ->> Provider: validate JWT with delegation evidence
    Provider -->>- Consumer: return access token
    Note over Consumer, Provider: Use some restricted service from Provider
    Consumer ->>+ Provider: /some/data/endpoint (incl. access token)
    Provider -->>- Consumer: return result
    deactivate Consumer
    
```


## Identity Provider
Only needed for human to machine interaction


## Policies
As I understand it, policies are only used in combination with delegation. 
An Entitled party has by definition the right to access all services/data provided by the Provider.
When an Entitled Party delegates to a Consumer, than a policy set is added to the Delegation Evidence.

A Policy set can contain the maximum depth of delegation.
When set, this indicates how many times a delegated party can delegate yet another Consumer.
It contains also one or more policies.

Each policy contains a target resource, 
actions (like READ or WRITE), 
an optional list of Service Providers,
and a set of rules.
Access to a resource is only permitted when there are all rules permit the resource, and non denying it.

Example of a policy set:
```json
{
   "policies": [
      {
         "target": {
            "resource": {
               "type": "CONTAINER.DATA",
               "identifiers": [
                  "ID.12345"
               ],
               "attributes": [
                  "CONTAINER.ETA"
               ]
            },
            "actions": [
               "iSHARE.READ"
            ],
            "environment": {
               "serviceProviders": [
                  "EU.EORI.NL567891234"
               ]
            }
         },
         "rules": [
            {
               "effect": "Permit"
            },
            {
               "effect": "Deny",
               "target": {
                  "resource": {
                     "type": "CONTAINER.DATA",
                     "identifiers": [
                        "ID.12378",
                        "ID.12379"
                     ],
                     "attributes": [
                        "CONTAINER.WEIGHT"
                     ]
                  }
               }
            }
         ]
      }
   ]
}
```



## Sources
- [Technical Documentation](https://dev.ishare.eu/)
- [Functional Documentation](https://ishareworks.atlassian.net/wiki)
- [OpenAPI/Swagger iShare Satellite](https://app.swaggerhub.com/apis/iSHARE/i-share_satellite/)
- [OpenAPI/Swagger Service Provider](https://w13.isharetest.net/)
- [OpenAPI/Swagger Entitled Party](https://banana.isharetest.net/swagger)
- [OpenAPI/Swagger Authorization Registry](https://ar.isharetest.net/swagger)
