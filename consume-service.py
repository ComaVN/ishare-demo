
from fastapi import FastAPI, HTTPException
from fastapi_healthcheck import HealthCheckFactory, healthCheckRoute

from python_ishare import create_jwt
from python_ishare import ISHARESatelliteClient

from consumer.config import *

import jwt
import logging
import http.client as http_client
import requests
import uuid


app = FastAPI()

# HTTP request Logging
http_client.HTTPConnection.debuglevel = 1
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True

# Add Health Checks
_healthChecks = HealthCheckFactory()
app.add_api_route('/health', endpoint=healthCheckRoute(factory=_healthChecks))



@app.get("/")
def read_root():
    requests_log.info('Root')
    my_token = token(provider_id)
    return my_token


@app.get("/sat/me")
def satellite_me():
    requests_log.info('(satellite) Me')
    access_token = fetch_access_token(satellite_base_url, satellite_id)
    response = requests.get(f"{satellite_base_url}/me",
                            headers={"Authorization": f"Bearer {access_token}"})
    return {"result": response.text}


@app.get("/sat/connect/token")
def satellite_connect_token():
    requests_log.info('(satellite) Access token')
    return fetch_access_token(satellite_base_url, satellite_id)


@app.get("/sat/capabilities")
def satellite_capabilities():
    requests_log.info('(satellite) Capabilities')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/capabilities",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'capabilities_token')}


@app.get("/sat/trusted_list")
def satellite_trusted_list():
    requests_log.info('(satellite) Trusted list')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/trusted_list",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'trusted_list_token')}


@app.get("/sat/parties")
def satellite_parties():
    requests_log.info('(satellite) Parties')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/parties",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'parties_token')}


@app.get("/sat/parties/{party_id}")
def satellite_party(party_id):
    requests_log.info(f'(satellite) Party {party_id}')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/parties/{party_id}",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'party_token')}


@app.get("/sat/versions")
def satellite_versions():
    requests_log.info('(satellite) Versions')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/versions",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'versions_token')}


@app.get("/sat/dataspaces")
def satellite_dataspaces():
    requests_log.info('(satellite) Dataspaces')
    access_token = fetch_access_token(satellite_base_url, satellite_id)

    response = requests.get(f"{satellite_base_url}/dataspaces",
                            headers={"Authorization": f"Bearer {access_token}"})

    return {"result": fetch_result_from_response(response, 'dataspace_list_token')}


@app.get("/pvd/capabilities")
def read_capabilities(access_token: str = None):
    if access_token:
        response = requests.get(f"{provider_base_url}/capabilities",
                                headers={"Authorization": f"Bearer {access_token}"})
    else:
        response = requests.get(f"{provider_base_url}/capabilities")

    return {"result": fetch_result_from_response(response, 'capabilities_token')}



@app.get("/pvd/me")
def read_me():
    access_token = fetch_access_token(provider_base_url, provider_id)
    response = requests.get(f"{provider_base_url}/me",
                            headers={"Authorization": f"Bearer {access_token}"})
    print(response.status_code)
    return {"result": response.text}


@app.get("/pvd/parkeerrechten")
def parkeerrechten(access_token: str):
    requests_log.info(f'parkeerrechten token: {access_token}')
    response = requests.get(f"{provider_base_url}/parkeerrechten",
                             headers={"Authorization": f"Bearer {access_token}"})
    requests_log.info(f'status code: {response.status_code}')
    if 200 <= response.status_code < 300:
        requests_log.info('return result')
        return {"result": response.text}
    requests_log.info('raise error')
    raise HTTPException(status_code=401)


@app.get("/pvd/connect/token")
def read_connect_token():
    requests_log.info('(provider) Access token')
    return fetch_access_token(provider_base_url, provider_id)


def token(target_id, token_id=None):
    return create_jwt(
        payload={
            "iss": id,
            "sub": id,
            "aud": target_id,
            "jti": token_id or str(uuid.uuid4())
        },
        private_key=private_key,
        x5c_certificate_chain=certificate_chain
    )


def fetch_access_token(base_url, server_id):
    response = requests.post(f"{base_url}/connect/token",
                         headers={
                             'Content-Type': 'application/x-www-form-urlencoded'
                         },
                         data={
                             'grant_type': 'client_credentials',
                             'scope': 'iSHARE',
                             'client_id': id,
                             'client_assertion_type': 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer',
                             'client_assertion': token(server_id)
                         }).json()
    requests_log.info(f'token response: {response}')
    return response['access_token']


def fetch_result_from_response(response, token_field):
    token = jwt.decode(response.json()[token_field], options={"verify_signature": False}, algorithms=["RS256"])
    return token


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=80)
